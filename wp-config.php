<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'revstare_fabric');
//define('DB_NAME', 'fibermil_mill');

/** MySQL database username */
define('DB_USER', 'root');
//define('DB_USER', 'fibermil_wpuser');

/** MySQL database password */
define('DB_PASSWORD', '');
//define('DB_PASSWORD', 'Test321$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
/**MEMORY DEFIN By AMIN KHAN*/
define('WP_MEMORY_LIMIT', '164M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' _BmiPP-*F5hKPha%pfaZtr3XGpz/*|/a-t51$gH7:CCvf`6H85|0r5W/Mz1ZW2Q');
define('SECURE_AUTH_KEY',  'VU]JA+$0>(/C11d>aDGyxdh(z,&Bk=-S:ESiGT[M88=3N(LQ-O82C3rw4.d=je#U');
define('LOGGED_IN_KEY',    '=.^!B_perMSsx`%a%q?^lpy~X}H6ISRRnBMxMm+2P>OvH=Vi}c2r?cP[<}cgV[rg');
define('NONCE_KEY',        'Onqa~&*HTF&S5> i7H[!<#t-w9jgMm2^z+kd2hPk,th#M)gcQ5%C`UJTHsd23q3I');
define('AUTH_SALT',        'z[:}kP^VLRF|]YEO,`f];DDtA(`+vu-!8`vz{Ruv!]wBZ_=1^iS&V_.6ue@w,;Z(');
define('SECURE_AUTH_SALT', 'HMxp#[#i%*[5kXeet&AJyzELN+h^|W#`~e#{jR<jO?xCQb|rEjDSuw7Q^+/|NL}M');
define('LOGGED_IN_SALT',   'hoa*2Bp,6nNIA7y]=||BjTbP!iuNYlq1eB#QE8hMyK&>MG?yXU$=5sg+MPi^MuF-');
define('NONCE_SALT',       'B&|YVkjDW><W~f~YLi@a;$W3O_+`:g 9d0O+-Pys8*xxU_s{9h}V>C2XNrLcpN-v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');